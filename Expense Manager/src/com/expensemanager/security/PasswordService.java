package com.expensemanager.security;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.expensemanager.exceptions.EncryptDecryptException;
import com.expensemanager.messages.ExceptionMessages;

import Decoder.BASE64Encoder;

public class PasswordService {
	
	private static PasswordService instance;
	 
	  public PasswordService()
	  {
	  }
	 
	  public synchronized String encrypt(String plaintext) throws EncryptDecryptException
	  {
	    MessageDigest md = null;
	    try
	    {
	      md = MessageDigest.getInstance("SHA"); 
	      md.update(plaintext.getBytes("UTF-8"));
	    }
	    catch(NoSuchAlgorithmException | UnsupportedEncodingException e)
	    {
	      throw new EncryptDecryptException(ExceptionMessages.ENCRYPTION_ERROR, e);
	    }
	 
	    byte raw[] = md.digest(); 
	    String hash = (new BASE64Encoder()).encode(raw); 
	    return hash; //step 6
	  }
	 
	  public static synchronized PasswordService getInstance() 
	  {
	    if(instance == null)
	    {
	       instance = new PasswordService();
	    }
	    return instance;
	  }

}
