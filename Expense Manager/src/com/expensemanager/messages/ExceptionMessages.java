package com.expensemanager.messages;

public class ExceptionMessages 
{
	public static final String ENCRYPTION_ERROR = "An error occured while encryption. ";
	public static final String DECRYPTION_ERROR = "An error occured while decryption. ";
	public static final String NO_TUPLE_FOUND = "No tuple found. ";
	public static final String UNABLE_TO_AUTHENTICATE_USER = "Unable to authenticate user. ";
	public static final String INSUFFICIENT_DATA = "Insufficient data to process the request . ";
	public static final String UNABLE_ADD_RECORD = "Unable add record into database.";

}
