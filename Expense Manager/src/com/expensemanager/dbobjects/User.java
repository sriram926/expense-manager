package com.expensemanager.dbobjects;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.expensemanager.exceptions.EncryptDecryptException;
import com.expensemanager.messages.ExceptionMessages;
import com.expensemanager.security.PasswordService;
import com.expensemanager.utils.DBUtils;

public class User {
	
	private String userId = null;
	private String fname = null;
	private String lname = null;
	private String email = null;
	private String phone = null;
	private String password = null;
	private int active = 0;
	
	public User(String userId, String fname, String lname, String email, String phone, String password, int active) 
	{
		this.userId = userId;
		this.fname = fname;
		this.lname = lname;
		this.email = email;
		this.phone = phone;
		this.password = password;
		this.active = active;
	}
	
	public void insert() throws SQLException, EncryptDecryptException
	{
		String query = "INSERT INTO USER VALUES(?,?,?,?,?,?,?)";
		PreparedStatement ps = DBUtils.getPreparedStatement(query, false);
		ps.setString(1, this.userId);
		ps.setString(2, this.fname);
		ps.setString(3, this.lname);
		ps.setString(4, this.email);
		ps.setString(5, this.phone);
		//PasswordService.getInstance().encrypt("this.password");
		ps.setString(6, PasswordService.getInstance().encrypt(this.password));
		ps.setInt(7, this.active);
		
		ps.execute();
	}
	
	public static User getUser(String userId) throws SQLException
	{
		String query = "SELECT userId, fname, lname, email, phone, Active FROM USER WHERE userId ='"+userId+"'";
		ResultSet rs = DBUtils.executeQuery(query);
		if(rs.next())
		{
			return new User(rs.getString("userId"), rs.getString("fname"), rs.getString("lname"), rs.getString("email"), rs.getString("phone"), null,rs.getInt("Active"));
		}
		return null;
	}
	
	public void update() throws SQLException, EncryptDecryptException
	{
		String query = "UPDATE user SET fname = ?, lname=?, email=?, phone=?, Active =? WHERE userid=?";
		PreparedStatement ps = DBUtils.getPreparedStatement(query, false);
		ps.setString(1, this.fname);
		ps.setString(2, this.lname);
		ps.setString(3, this.email);
		ps.setString(4, this.phone);
		ps.setInt(5, this.active);
		ps.setString(6, this.userId);
		
		ps.execute();
	}

	public void updatePassword(String oldPwd, String newPwd) throws SQLException
	{
//		if(oldPwd != null && newPwd != null && authenticateUser(this.userId, oldPwd))
		{
			String query = "UPDATE user SET password = ? WHERE userid=?";
			PreparedStatement ps = DBUtils.getPreparedStatement(query, false);
			ps.setString(1, newPwd);
			ps.setString(2, this.userId);
			ps.execute();
		}
		
	}
	public static boolean authenticateUser(String userId, String password) throws SQLException
	{
		String query = "SELECT password FROM USER WHERE userId ='"+userId+"'";
		ResultSet rs = DBUtils.executeQuery(query);
		if(rs.next())
		{
			try
			{
				return PasswordService.getInstance().encrypt(password).equals(rs.getString("password"));
			}
			catch(EncryptDecryptException e)
			{
				throw new SQLException(ExceptionMessages.UNABLE_TO_AUTHENTICATE_USER, e);
			}
		}
		return false;
	}
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}
}
