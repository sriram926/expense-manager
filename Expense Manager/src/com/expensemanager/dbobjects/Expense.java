package com.expensemanager.dbobjects;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.expensemanager.exceptions.EncryptDecryptException;
import com.expensemanager.messages.ExceptionMessages;
import com.expensemanager.security.PasswordService;
import com.expensemanager.utils.DBUtils;

public class Expense {

	private int expense_Id ;
	private String description ; 
	private int amount; 
	private String currency ; 
	private String category;
	private String type;
	private Date expense_date; 
	private String notes;

	private String payer_id;
	private String payee_ids[];
	
	public Expense(String description, int amount, String currency,
			String category, String type, Date expense_date, String notes, String payer_id, String payee_ids[]) 
	{
		this.description = description;
		this.amount = amount;
		this.currency = currency;
		this.category = category;
		this.type = type;
		this.expense_date = expense_date;
		this.notes = notes;
		
		this.payer_id = payer_id;
		this.payee_ids =payee_ids;
	}
	
	public void insert() throws SQLException, EncryptDecryptException
	{
		String query = "INSERT INTO EXPENSE(description, amount, currency, category, type, expense_date, notes) VALUES(?,?,?,?,?,?,?)";
		PreparedStatement ps = DBUtils.getPreparedStatement(query, true);
		ps.setString(1, this.description);
		ps.setInt(2, this.amount);
		ps.setString(3, this.currency);
		ps.setString(4, this.category);
		ps.setString(5, this.type);
		//PasswordService.getInstance().encrypt("this.password");
		ps.setDate(6, this.expense_date);
		ps.setString(7, this.notes);
		
		ps.execute();
		
		ResultSet rs = ps.getGeneratedKeys();
		
		if(rs.next())
		{
			int expense_id = rs.getInt(1);
			int noOfUsers = payee_ids.length;
			int share_percentage = 100/payee_ids.length;

			for(int i=0; i<noOfUsers; i++)
			{
				String each_payee = payee_ids[i];
				User_Expense eachUser= new User_Expense(expense_id, payer_id, each_payee, share_percentage, 1);
				eachUser.insert();
			}
		}
		else 
		{
			throw new SQLException(ExceptionMessages.UNABLE_ADD_RECORD);
		}
	}
	
	public static Expense getExpenseDetails(String expenseId)
	{
		String query = "SELECT *";
		return null;
	}
}
