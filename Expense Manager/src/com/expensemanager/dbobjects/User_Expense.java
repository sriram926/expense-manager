package com.expensemanager.dbobjects;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.expensemanager.utils.DBUtils;

public class User_Expense 
{
	private int expense_id; 
	private String payer_id;
	private String payee_id ;
	private float share_percentage;
	private int Active ;
	
	
	public User_Expense(int expense_id, String payer_id, String payee_id,
			float share_percentage, int active) 
	{
		this.expense_id = expense_id;
		this.payer_id = payer_id;
		this.payee_id = payee_id;
		this.share_percentage = share_percentage;
		Active = active;
	}
	
	public void insert() throws SQLException
	{
		String query = "INSERT INTO USER_EXPENSE VALUES(?,?,?,?,?)";
		PreparedStatement ps = DBUtils.getPreparedStatement(query, false);
		ps.setInt(1, this.expense_id);
		ps.setString(2, this.payer_id);
		ps.setString(3, this.payee_id);
		ps.setFloat(4,  this.share_percentage);
		ps.setInt(5, this.Active);
		
		ps.execute();
	}
	
	public static User_Expense getUser_Expense(int expenseId, String payerId, String payeeId) throws SQLException
	{
		StringBuilder query = new StringBuilder("SELECT expense_Id, payer_id, payee_id, share_percentage,Active "
				+ "FROM user_expense "
				+ "WHERE expense_Id ="+expenseId+" and payer_id='"+payerId+"'");
				
		if(payeeId != null)
		{
			query.append(" and payee_id='"+payeeId+"'");
		}
		
		ResultSet rs = DBUtils.executeQuery(query.toString());
		if(rs.next())
		{
			return new User_Expense(rs.getInt("expense_id"), rs.getString("payer_id"), rs.getString("payee_id"), rs.getFloat("share_percentage"), rs.getInt("active"));
		}
		return null;
	}
	
	
}
