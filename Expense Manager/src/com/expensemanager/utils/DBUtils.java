package com.expensemanager.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class DBUtils {

	public static ResultSet executeQuery(String query)
	{
		Connection connection = DBConnection.getConnection();
		Statement stmt;
		ResultSet rs = null;
		try 
		{
			stmt = connection.createStatement();
			rs =  stmt.executeQuery(query);
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}
	
	public static PreparedStatement getPreparedStatement(String query, boolean generate_keys)
	{
		PreparedStatement ps = null;
		try
		{
			if(generate_keys)
			{
				ps = DBConnection.getConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			}
			else
			{
				ps = DBConnection.getConnection().prepareStatement(query);
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		return ps;
	}
	
	public static java.sql.Date getCurrentDate() 
	{
	    java.util.Date today = new java.util.Date();
	    return new java.sql.Date(today.getTime());
	}
}

class DBConnection
{
	private static String DB_USER = "root";
	private static String DBA_PWD = "sriram";
	private static String DEFAULT_SCHEMA = "expense_manager";
	private static String DB_SERVER_URL = "jdbc:mysql://localhost:3306/";
	private static String DB_DRIVER = "com.mysql.jdbc.Driver";
	static Connection conn = null;
	
		protected DBConnection() {
	      // Exists only to defeat instantiation.
		}
	   public static Connection getConnection() 
	   {
	      if(conn == null) 
	      {
	    	  try 
	    	  {
	    		  Class.forName(DB_DRIVER);
	    		  conn = DriverManager.getConnection(DB_SERVER_URL+DEFAULT_SCHEMA, DB_USER, DBA_PWD);
	    	  } 
	    	  catch (ClassNotFoundException | SQLException e) 
	    	  {
	    		  e.printStackTrace();
	    		  return null;
	    	  }
	      }
	      return conn;
	   }
	   
}
