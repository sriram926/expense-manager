package com.expensemanager.exceptions;

public class EncryptDecryptException extends Exception 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EncryptDecryptException() {

	}
	
	public EncryptDecryptException(String message)
	{
		super(message);
	}
	
	public EncryptDecryptException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
